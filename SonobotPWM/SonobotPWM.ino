#include <XBee.h>
#include <NeoSWSerial.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>

#define OFF       0
#define MANUAL    1
#define AUTO      2

#define CurrentL  A0
#define CurrentR  A1

//TODO : Try and See if Auto Discovery of BaseStation is possible
// Read other Status replies for TX messages
// Integrate RESET of XBEE to Arduino
// Check if spretrum alloted is over and reset XBEE


//Sensor is 100A bi-directional = 20

/* Offsets
If bi-directional = 2500
If uni- directional = 600
*/
int mVperAmp = 40;
int ACSoffset = 2492; // See offsets below
int RawValue = 0;
double Voltage = 0;
double AmpsL = 0 , AmpsR = 0;
char AmpStrL[7] , AmpStrR[7];
float slope_A = 2.1;

void loop()  __attribute__((__optimize__("Os")));

#define RESPONSE_SIZE_EXCEEDED  -5
#define RESPONSE_NOT_OK         -4
#define RESPONSE_INCORRECT      -3
#define AT_COMMAND_FAILED       -2
#define UNKNOWN_ERROR           -1

#define Header1     0x3f              //Header 1 for Manual control Message
#define Header2     0x87              //header 2 for Manucal control message

#define Base            0x0013A20041887F3B
#define Control        0x0013A20041887F44


NeoSWSerial mySerial( 2, 3 );
uint8_t GotData = 0;
uint8_t Mode = OFF , times = 0;

XBee xbee = XBee();
char *ReadData = NULL , Data[100];
uint8_t  Timer = 0 , PacketNo = 0 , Timeout = 0 , SendBase = 0;;
uint16_t MotorL = 0 , MotorR = 0;

char GPS[100];
uint8_t SendGPS = 0;
uint8_t index = 0;

static void handleRxChar( uint8_t c ){    //RX char handler for SW serial
  if(c == '$' && SendGPS == 0 ){
    index = 0;
    memset(GPS , 0 , sizeof(GPS));
    GPS[index++] = (char)c;
  }else if(c == '\n' && SendGPS == 0){
    GPS[index++] = (char)c;
    SendGPS = 1;
  }else if(index > 0 && SendGPS == 0){
    GPS[index++] = (char)c;
  }
}


void setupPWM16() {
    DDRB |= _BV(PB1) | _BV(PB2);        /* set pins as outputs */
    TCCR1A = _BV(COM1A1) | _BV(COM1B1)  /* non-inverting PWM */
        | _BV(WGM11);                   /* mode 14: fast PWM, TOP=ICR1 */
    TCCR1B = _BV(WGM13) | _BV(WGM12) | _BV(CS11);                    /* no prescaling */
    ICR1 = 40000;                      /* TOP counter value for 50Hz*/
    OCR1A = 0;
    OCR1B = 0;
}
/* 16-bit version of analogWrite(). Works only on pins 9 and 10. */
void analogWrite16(uint8_t pin, uint16_t val){
    switch (pin) {
        case  9: OCR1A = val; break;
        case 10: OCR1B = val; break;
    }
}

ISR(TIMER2_OVF_vect){   //Timer 2 Overflow interrupt
    Timer++;
    Timeout++;
}

void timer2_init(){ // Around 33ms timer 
  cli();
   TIMSK2 = (TIMSK2 & B11111110) | 0x01;
   TCCR2B = (TCCR2B & B11111000) | 0x07;
  sei();
}

int8_t SendDataXbee(uint64_t Addresss , char *PayLoad , uint8_t Length, uint8_t Retries , uint16_t Timeout) { //Send Data to XBEE
  int returnVal = 0;
  uint8_t retry = 0;

  XBeeAddress64 addr64 = XBeeAddress64(Addresss);
  ZBTxRequest zbTx = ZBTxRequest(addr64, (uint8_t*) PayLoad, Length);
  ZBTxStatusResponse txStatus = ZBTxStatusResponse();
  
//  mySerial.println("Inside Send Data");
  while (retry <= Retries) {
    retry++;
//    mySerial.println("Sent Data");
    xbee.send(zbTx);
    if (xbee.readPacket(Timeout)) {
      // got a response!
      // should be a znet tx status
//      mySerial.println("Got a Response in Send Data");
      if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
        xbee.getResponse().getZBTxStatusResponse(txStatus);
        // get the delivery status, the fifth byte
        if (txStatus.getDeliveryStatus() == SUCCESS) {
//          mySerial.println("Delivery Sucess");
          return 0;
        } else {
//          mySerial.println("Failure Send Data");
          returnVal = -1;
        }
      }else{
//        mySerial.println("Delivery Status ");
     }
    } else if (xbee.getResponse().isError()) {
//      mySerial.println("Error -2");
      returnVal = -2;
    } else {
      // local XBee did not provide a timely TX Status Response -- should not happen
//      mySerial.println("Error -3");
      returnVal = -3;
    }
  }
  return returnVal;
}


char *ReadDataXbee() {          //Read data from XBEE
  static char Buffer[35];
  char * BufferAdd;
  BufferAdd = Buffer;
  XBeeResponse response = XBeeResponse();
  // create reusable response objects for responses we expect to handle
  ZBRxResponse rx = ZBRxResponse();
  memset(Buffer , 0 , sizeof(Buffer));
  xbee.readPacket();
  if (xbee.getResponse().isAvailable()) {
    // got something
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      // got a zb rx packet
      // now fill our zb rx class
      xbee.getResponse().getZBRxResponse(rx);
      if (rx.getOption() == ZB_PACKET_ACKNOWLEDGED) {
      } else {
      }
      for (int i = 0; i < rx.getDataLength(); i++) {
        Buffer[i] = rx.getData()[i];
      }
      return BufferAdd;
    }
  } else if (xbee.getResponse().isError()) {
    return NULL;
  }
  return NULL;
}

void setup() {
  // put your setup code here, to run once:
  setupPWM16();                               //Setup 16Bit PWM for Motors
  timer2_init();                              //Setup 8bit Timer for Sending data
  Serial.begin(115200);                       //Setup HW serial for XBEE
  xbee.setSerial(Serial);                     // Initialise XBEE
  
  mySerial.begin(38400);                      //Setup SW serial for raspberry pi communication
  mySerial.attachInterrupt( handleRxChar );   // Add RX data handler over SW serial.
}

void loop()  {
  // put your main code here, to run repeatedly:

  memset(AmpStrL , 0 , sizeof(AmpStrL));
  memset(AmpStrR , 0 , sizeof(AmpStrR));
  //Read Current Here
  RawValue = 0;  // Read and Calculate the Current for Right motor channel
  for(int i = 0 ; i < 20 ; i++){
    RawValue += analogRead(CurrentR);
  }
  Voltage = ((RawValue / 20) / 1023.0) * 5000; // Gets you mV
  AmpsR =  ((Voltage - ACSoffset) / mVperAmp);
  AmpsR = (slope_A * AmpsR);
  dtostrf(AmpsR , 6 , 2, AmpStrR);

   RawValue = 0;    // Read and Calculte the Current for Left motor channel
  for(int i = 0 ; i < 20 ; i++){
    RawValue += analogRead(CurrentL);
  }
  Voltage = ((RawValue/20) / 1023.0) * 5000; // Gets you mV
  AmpsL =  ((Voltage - ACSoffset) / mVperAmp);
  AmpsL = (slope_A * AmpsL);
  dtostrf(AmpsL , 6 , 2, AmpStrL);  

  ReadData = ReadDataXbee();  //See if There is data on XBEE
  if(ReadData !=  NULL && ReadData[0] != Header1 &&!strncmp(ReadData , "STARTDATA" , 8)){   //START DATA command from BaseStation
    SendBase = 1;
  }else if(ReadData !=  NULL && ReadData[0] != Header1 && !strncmp(ReadData , "STOPDATA" , 7)){ //STOP DATA command from BaseStation
    SendBase = 0;
  }else if (ReadData != NULL && (Mode == MANUAL || Mode == OFF)) {    //If RAW data received from Handheld controller decode and put to PWM registers
      MotorL = (word(ReadData[2], ReadData[3]));
      MotorR = (word(ReadData[4], ReadData[5]));
        analogWrite16(9, MotorL);
        analogWrite16(10, MotorR);
        //Send Reply to Data 
        times++;
        if(times > 5){
          memset(Data , 0 , sizeof(Data));
          strcpy(Data , "Thank You");
          SendDataXbee(Control , Data , 9 , 1 , 100 );
          times = 0;
        }
        Mode = MANUAL;
      Timeout = 0;
  }
  if(SendGPS  && SendBase > 0 ){  //Whenever GPS Data is recieved from Raspberry pi send to BaseStation
    SendDataXbee(Base , GPS , index , 1  , 100 );
    SendGPS = 0;
  }
 if(Timer > 15){   // Send Data to Raspberry pi every 250ms for logging
    memset(Data , 0 , sizeof(Data));
    sprintf(Data , "$EVOLOGICS,%s,%s,%04d,%04d,%03d\r\n", AmpStrR , AmpStrL , MotorR , MotorL , PacketNo);
    mySerial.print(Data);
    if(SendBase > 0){
      SendDataXbee(Base , Data , 44 , 1  , 100 );
    }
    PacketNo++;
    Timer = 0;
 }
 
 if(Timeout > 14){ // If no packet from Handheld Recieved for 462ms stop the PWM
  MotorL = 0;
  MotorR = 0;
  analogWrite16(9, MotorL);
  analogWrite16(10, MotorR);
  Mode = OFF;
 }
}
