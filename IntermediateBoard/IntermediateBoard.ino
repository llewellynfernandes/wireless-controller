
#define Manual    5
#define Connect   6

uint8_t ConnectPos = 1;
uint8_t ManualPos = 1;

uint8_t pos1 , pos2;

ISR(TIMER2_OVF_vect) { //ISR on Compare Match A
  pos1++;
  pos2++;

  if(pos1 == 1){
    digitalWrite(10 , HIGH);
  }
  if(pos1 == (ManualPos + 1)){
    digitalWrite(10 , LOW);
  }
  if(pos1 == 19){
    pos1 = 0;
  }

  if(pos2 == 7){
    digitalWrite(9 , HIGH);
  }

  if(pos2 == (ConnectPos + 7)){
    digitalWrite(9 , LOW);
  }

  if(pos2 == 25){
    pos2 = 6 ;
  }
  TCNT2 = 120;           //Reset Timer to 130 out of 255
  TIFR2 = 0x00;          //Timer2 INT Flag Reg: Clear Timer Overflow Flag
}

void setup() {
  // put your setup code here, to run once:
  pinMode(Manual , INPUT);
  pinMode(Connect , INPUT);

  pinMode(9 , OUTPUT);
  pinMode(10 , OUTPUT);
  pos1 = 0; 
  pos2 = 0;

  cli();

  TCCR2B = 0x00;        //Disbale Timer2 while we set it up
  TCNT2  = 130;         //Reset Timer Count to 130 out of 255
  TIFR2  = 0x00;        //Timer2 INT Flag Reg: Clear Timer Overflow Flag
  TIMSK2 = 0x01;        //Timer2 INT Reg: Timer2 Overflow Interrupt Enable
  TCCR2A = 0x00;        //Timer2 Control Reg A: Wave Gen Mode normal
  TCCR2B = 0x05;        //Timer2 Control Reg B: Timer Prescaler set to 12to 0

  sei();

}

void loop() {
  // put your main code here, to run repeatedly:

  if (digitalRead(Manual) == LOW) {
    ManualPos = 1;
  } else {
    ManualPos = 2;
  }
  if (digitalRead(Connect) == LOW) {
    ConnectPos = 1;
  } else {
    ConnectPos = 2;
  }
}
