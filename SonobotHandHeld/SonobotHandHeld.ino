#include <XBee.h>
#include <SoftwareSerial.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>

//TOD0 : Add ID to EEPROM
//TODO : Add Discovery (Possibly)

#define Rover                    0x0013A20041887F4C  

//TODo : Change the Way single axis joystick is used
//Use it as a Multiplier for the RAW Value
//Read error codes from send data to xbee
//integrate the RESET pin from XBEE
//Check Alloted spectrum error. and reset xbee if needed


#define RESPONSE_SIZE_EXCEEDED  -5
#define RESPONSE_NOT_OK         -4
#define RESPONSE_INCORRECT      -3
#define UNKNOWN_ERROR           -1

#define MANUAL_LED               2
#define AUTO_LED                 3
#define SW                       4
#define STATUS_LED               5

#define XAXIS                    A0
#define YAXIS                    A1
#define SCALE                    A2

#define Header1                  0x3f
#define Header2                  0x87

#define ANALOG_SAMPLES           20
#define MAX                      405

void loop()  __attribute__((__optimize__("Os")));

SoftwareSerial mySerial(2, 3); // RX, TX
XBee xbee = XBee();


char *ReadData = NULL;
unsigned long currentMillis = 0;
uint16_t BaseValueX = 0 , BaseValueY = 0;
int ValueX = 0 ,  ValueY = 0 , ValueS = 0;
uint16_t MotorL = 0 , MotorR = 0 , Speed = 0;
char Packet[6];
uint8_t timer = 0 , Led = 0;

volatile byte state = LOW;
volatile byte Mode = HIGH;

int8_t SendDataXbee(uint64_t Addresss , char *PayLoad , uint8_t Length, uint8_t Retries , uint16_t Timeout) {
  int returnVal = 0;
  uint8_t retry = 0;

  XBeeAddress64 addr64 = XBeeAddress64(Addresss);
  ZBTxRequest zbTx = ZBTxRequest(addr64, (uint8_t*) PayLoad, Length);
  ZBTxStatusResponse txStatus = ZBTxStatusResponse();
  while (retry <= Retries) {
    retry++;
    xbee.send(zbTx);
    if (xbee.readPacket(Timeout)) {
      // got a response!
      // should be a znet tx status
      if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
        xbee.getResponse().getZBTxStatusResponse(txStatus);
        // get the delivery status, the fifth byte
        if (txStatus.getDeliveryStatus() == SUCCESS) {
          return 0;
        } else {
          returnVal = -1;
        }
      }
    } else if (xbee.getResponse().isError()) {
      returnVal = -2;
    } else {
      // local XBee did not provide a timely TX Status Response -- should not happen
      returnVal = -3;
    }
  }
  return returnVal;
}


char *ReadDataXbee() {
  static char Buffer[35];
  char * BufferAdd;
  BufferAdd = Buffer;
  XBeeResponse response = XBeeResponse();
  // create reusable response objects for responses we expect to handle
  ZBRxResponse rx = ZBRxResponse();
  memset(Buffer , 0 , sizeof(Buffer));
  xbee.readPacket();
  if (xbee.getResponse().isAvailable()) {
    // got something
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      // got a zb rx packet
      // now fill our zb rx class
      mySerial.println("HelloWorld");
      xbee.getResponse().getZBRxResponse(rx);
      if (rx.getOption() == ZB_PACKET_ACKNOWLEDGED) {
        // the sender got an ACK
      } else {
      }
      for (int i = 0; i < rx.getDataLength(); i++) {
        Buffer[i] = rx.getData()[i];
      }
      return BufferAdd;
    }
  } else if (xbee.getResponse().isError()) {
    return NULL;
  }
  return NULL;
}

ISR(TIMER2_OVF_vect){ //Timer 2 Overflow interrupt
    Led++;
    timer++;
}

void timer2_init(){ //30 ms timer 
  cli();
   TIMSK2 = (TIMSK2 & B11111110) | 0x01;
   TCCR2B = (TCCR2B & B11111000) | 0x07;
  sei();
}

void setup() {
  // put your setup code here, to run once:
  // Setup GPIO
  pinMode(MANUAL_LED , OUTPUT);
  pinMode(AUTO_LED , OUTPUT);
  pinMode(STATUS_LED , OUTPUT);
  pinMode(SW , INPUT);
  Serial.begin(115200);   //Setup serial for XBEE
  mySerial.begin(9600);   //Software Serial for Debug
  xbee.setSerial(Serial); //Tell XBEE which serial to use

  timer2_init();                                  
  for(uint8_t i = 0 ; i <  ANALOG_SAMPLES ; i++){  // Read Zero Values of Joystick
    BaseValueX += analogRead(XAXIS);
    BaseValueY += analogRead(YAXIS);
  }
  BaseValueX = BaseValueX / ANALOG_SAMPLES;
  BaseValueY = BaseValueY / ANALOG_SAMPLES;
}


void loop() {
  // put your main code here, to run repeatedly:
  ValueX = analogRead(XAXIS) - BaseValueX; //Read X axis of Joystick
  ValueY = analogRead(YAXIS) - BaseValueY; //Read Y axis of Joystick
  ValueS = analogRead(SCALE);              //Read Y Axis of 2nd Joystick

   //Calculate values for MotorL and MotorR based on Joysticks
  Speed = map(ValueY , -1 * MAX , MAX , ValueS , -1 * ValueS);
  MotorL = map(ValueX , 0 ,  MAX  , 3000 , 4000) -  Speed; 
  MotorR = map(ValueX , -1 * MAX ,  0  , 4000 , 3000) -  Speed;

  if(MotorL < 2000){              //Just a Safegaurd for Lower limit of Motor
    MotorL = 2000;
    
  }else if(MotorL > 4000){        //Just a Safegaurd for upper limit of Motor
    MotorL = 4000;
  }
   if(MotorR < 2000){
    MotorR = 2000;
    
  }else if(MotorR > 4000){
    MotorR = 4000;
  }
  digitalWrite(STATUS_LED, state);
  if(timer > 2){
    if(Mode == LOW){   //If Mode is manual then Packetise data then send to XBEE
      //Send Packet Here
      Packet[0] = Header1;
      Packet[1] = Header2;
      Packet[2] = highByte(MotorL);
      Packet[3] = lowByte(MotorL);
      Packet[4] = highByte(MotorR);
      Packet[5] = lowByte(MotorR);
      SendDataXbee(Rover , Packet , 6 , 2  , 500 );
    }
    timer = 0;
  }
  if(Led > 30){                         //Blink LED if NO data recieved from Rover for 1000ms
    if((millis() - currentMillis) > 1000){
      state = !state;
    }else{
      state = 1;
    }
    Led = 0;
  }

  if(!digitalRead(SW)){               //Check toggle switch for Mode
    digitalWrite(MANUAL_LED, HIGH);
    digitalWrite(AUTO_LED, LOW);
    Mode = LOW;
  }else{
    digitalWrite(MANUAL_LED, LOW);
    digitalWrite(AUTO_LED, HIGH);
    Mode = HIGH;
  }
 
  ReadData = ReadDataXbee();                //Read Data from Xbee for response from Rover
  if (ReadData != NULL && !strncmp(ReadData , "Thank You" , 8)) { //Compare Recieved string
    //Do something With the Data
    currentMillis = millis();
    //Send Reply to Data 
  }
}
