#include "controller.h"

SoftwareSerial mySerial(SRX, STX); // RX, TX
FastCRC8 CRC8;

//Initialise all the Pins here
void InitHardware() {
  pinMode(SW , INPUT);
  pinMode(ESTOP , INPUT);
  pinMode(MLED , OUTPUT);
  pinMode(ALED , OUTPUT);
  pinMode(SLED , OUTPUT);
  pinMode(VRLED , OUTPUT);
  pinMode(VGLED , OUTPUT);
  pinMode(XRST , INPUT);
  digitalWrite(MLED , LOW);
  digitalWrite(ALED , LOW);
  digitalWrite(SLED , LOW);
  digitalWrite(VRLED , LOW);
  digitalWrite(VGLED , LOW);
  mySerial.begin(9600);
}

bool ToggleLED(void *t) {
  Controller *tm = (Controller *)t;

  if ((millis() - tm->LastReply) > 4000 && tm->EEPROMSuccess == 1) {
    digitalWrite(SLED, !digitalRead(SLED)); // toggle the LED
  } else {
    digitalWrite(SLED, HIGH); // toggle the LED
  }
  return true; // repeat? true
}

bool VoltageMonitor(void *t) {
  Controller *tm = (Controller *)t;
  if (tm->EEPROMSuccess == 1) {
    if (tm->Battery < 7.0) {
      digitalWrite(VRLED , !digitalRead(VRLED));
      digitalWrite(VGLED , LOW);
    } else if (tm->Battery < 7.4 && tm->Battery > 7.0) {
      digitalWrite(VRLED , HIGH);
      digitalWrite(VGLED , LOW);
    } else if (tm->Battery < 8.0 && tm->Battery > 7.4) {
      digitalWrite(VGLED , !digitalRead(VGLED));
      digitalWrite(VRLED , LOW);
    } else if (tm->Battery > 8.0) {
      digitalWrite(VGLED , HIGH);
      digitalWrite(VRLED , LOW);
    }
  }
  return true; // repeat? true
}


uint16_t ReadADC(uint8_t Pin) {
  uint16_t Temp = 0;
  for (uint8_t i = 0 ; i <  ANALOG_SAMPLES ; i++) { // Read Zero Values of Joystick
    Temp += analogRead(Pin);
  }
  return (Temp / ANALOG_SAMPLES);
}


void GetCallibration(Controller *tm) {
  tm->BaseValueX =  ReadADC(XAXIS);
  tm->BaseValueY =  ReadADC(YAXIS);
}


bool ReadAllHardware(void *t) {
  static uint8_t rate = 0;
  Controller *tm = (Controller *)t;

  if (tm->EEPROMSuccess == 1) {
    //Read Analog Values for Control Joysticks
    tm->ValueX = ReadADC(XAXIS) - tm->BaseValueX;
    tm->ValueY = ReadADC(YAXIS) - tm->BaseValueY;
    tm->ValueS = ReadADC(SCALE);

    tm->Speed = map(tm->ValueY , -1 * MAX , MAX , tm->ValueS , -1 * tm->ValueS);
    tm->MotorL = map(tm->ValueX , 0 ,  MAX  , 3000 , 4000) -  tm->Speed;
    tm->MotorR = map(tm->ValueX , -1 * MAX ,  0  , 4000 , 3000) -  tm->Speed;

    //Read Digital Switch values
    tm->Switch = digitalRead(SW);
    tm->EStop = digitalRead(ESTOP);

    //Read Voltage
    tm->Battery = ( (((double)ReadADC(VI) / 1024.0) * 3.3) * ((10.0 + 3.0) / 3.0));
    tm->JoystickVTG = ( (((double)ReadADC(JVoltage) / 1024.0) * 3.3) * ((10.0 + 19.6) / 19.6));


    memset(tm->RFData , 0 , sizeof(tm->RFData));
    tm->RFData[0] = PACKETHEADER;
    tm->RFData[1] = MANUALCONTROL;

    if (tm->EStop == HIGH) {
      rate++;
      if (rate > 2) {
        digitalWrite(MLED , !digitalRead(MLED));
        digitalWrite(ALED , !digitalRead(ALED));
        rate = 0;
      }
    } else if (tm->Switch == LOW) {
      tm->Mode = AUTO;
      digitalWrite(MLED , LOW);
      digitalWrite(ALED , HIGH);
      tm->RFData[2] = 0;
      tm->RFData[3] = 0;
      tm->RFData[4] = 0;
      tm->RFData[5] = 0;
    } else if (tm->Switch == HIGH) {
      tm->Mode = MANUAL;
      digitalWrite(MLED , HIGH);
      digitalWrite(ALED , LOW);
      tm->RFData[2] = highByte(tm->MotorL);
      tm->RFData[3] = lowByte(tm->MotorL);
      tm->RFData[4] = highByte(tm->MotorR);
      tm->RFData[5] = lowByte(tm->MotorR);
    }
    tm->RFData[6] = tm->EStop;    //ESTOP
    tm->RFData[7] = CRC8.smbus(tm->RFData , sizeof(tm->RFData) - 1);    //CRC
  } else if (tm->EEPROMSuccess == 120) {
    digitalWrite(MLED , HIGH);
    digitalWrite(ALED , HIGH);
    digitalWrite(SLED , HIGH);
    digitalWrite(VRLED , HIGH);
    digitalWrite(VGLED , HIGH);
  }
  return true; // repeat? true
}

bool SendRFData(void *t) {
  static uint8_t rate = 0;
  const uint8_t Buffer[2] = {0x11, 0x22 };    //HeartBeat
  Controller *tm = (Controller *)t;
  if (tm->EEPROMSuccess == 1) {
    if ((tm->Mode == MANUAL || tm->EStop == HIGH)) {
      //Send RF Data Here
      Transmit(((uint64_t)(tm->RoverAddressH) << 32  | tm->RoverAddressL ) , tm->RFData , sizeof(tm->RFData));
    }
    if ((rate == 0 || rate > 10) && tm->Mode == AUTO && tm->EStop == LOW ) {
      Transmit(((uint64_t)(tm->RoverAddressH) << 32  | tm->RoverAddressL ) , (uint8_t*)Buffer , 2);
      rate = 1;
    }
    rate++;
  }
  if (tm->SendResponse) {
    Transmit(tm->ResponseID , tm->ResponseData , sizeof(tm->ResponseData));
    tm->SendResponse = 0;
  }
  return true; // repeat? true
}
