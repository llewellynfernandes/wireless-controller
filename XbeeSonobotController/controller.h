/*
 * board.h
 *
 *  Created on: Oct 22, 2018
 *      Author: llewellyn
 */

#ifndef SRC_CONTROLLER_H_
#define SRC_CONTROLLER_H_

#include "board.h"
#include <stdint.h>
#include <Arduino.h>
#include <FastCRC.h>
#include <SoftwareSerial.h>
#include <XBee.h>

typedef struct{
  uint16_t BaseValueX, BaseValueY; //Callibration Values for X and Y Asix
  int ValueX ,ValueY, ValueS;      //Values for Controll Joystick and Pot
  uint8_t Switch , EStop;          //Values for Mode SW and ESTOP switch
  float Battery , JoystickVTG;     // Main Battery Voltage and Joystick Voltage
  uint8_t Mode;                   //Mode of the Controller
  unsigned long LastReply;        //Last Reply time from Sonobot
  uint16_t MotorL , MotorR , Speed;
  uint8_t RFData[8];              //Buffer to Send RF Data
  uint32_t RoverAddressH , RoverAddressL;
  uint8_t ResponseData[10];
  XBeeAddress64 ResponseID;
  uint8_t SendResponse;
  uint8_t EEPROMSuccess;
}Controller;



void InitHardware();
void GetCallibration(Controller *tm);
uint16_t ReadADC(uint8_t Pin);
bool ReadAllHardware(void *t);
bool ToggleLED(void *t);
bool SendRFData(void *t);
bool VoltageMonitor(void *t);


extern void Transmit(uint64_t Address , uint8_t *Data , uint8_t Length);
extern FastCRC8 CRC8;
extern void ReadEEPROM(Controller *tm);

#endif /* SRC_CONTROLLER_H_ */
