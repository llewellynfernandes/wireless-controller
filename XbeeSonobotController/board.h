/*
 * board.h
 *
 *  Created on: Oct 22, 2018
 *      Author: llewellyn
 */

#ifndef SRC_BOARD_H_
#define SRC_BOARD_H_

#define VERSION           1.0
#define ANALOG_SAMPLES    20

#define PACKETHEADER      0x3f 
#define MANUALCONTROL     0xAB 

#define XAXIS             A1
#define YAXIS             A0
#define SCALE             A2
#define JVoltage          A3
#define VI                A4

#define XRST              A5
#define ALED              4
#define MLED              5
#define SLED              6
#define VRLED             10
#define VGLED             9

#define ESTOP             7
#define SW                8

#define STX               2
#define SRX               3

#define AUTO              10
#define MANUAL            11

#define MAX               405

#endif /* SRC_BOARD_H_ */
