#include "controller.h"
#include "rf.h"

Controller control;

void loop()  __attribute__((__optimize__("Os")));
uint8_t ResponseData[5]  = {PACKETHEADER , 0xAA , 0xBB , 0xCC , 0x06};

void setup() {
  InitHardware();
  InitXBEE(&control);
}

void loop() {
  XLoop();                      //Loop for Xbeee callbacks
  
  if ((millis() - control.LastUpdate) > 1000) {
    analogWrite16(LEFTMOTOR, 0);
    analogWrite16(RIGHTMOTOR , 0);
    control.Mode = AUTO;
    digitalWrite(MANUALEN, LOW);
  } else {
    control.Mode = MANUAL;
    digitalWrite(MANUALEN, HIGH);
  }

  if ((millis() - control.LastHeartBeat ) > 5000) {
    digitalWrite(CONNECT, LOW);
  } else {
    digitalWrite(CONNECT, HIGH);
  }
  if (control.SendResponse > 6) {
    Transmit(control.ResponseID , (uint8_t *) ResponseData , 5);
    control.SendResponse = 0;
  }
  
  if (control.SendIMC) {
    for (uint8_t i = 0; i < control.IMCLength ; i++) {
      mySerial.write(control.RFData[i]);
    }
    control.LastIMC = millis();
    memset(control.RFData , 0 , sizeof(control.RFData));
    control.IMCLength = 0;
    control.SendIMC = 0;
  }

  if (control.Mode == MANUAL) {
    if (control.EStop == HIGH) {
      analogWrite16(LEFTMOTOR, 3000);
      analogWrite16(RIGHTMOTOR, 3000);
    } else if (control.EStop == LOW) {
      analogWrite16(LEFTMOTOR, control.MotorL);
      analogWrite16(RIGHTMOTOR, control.MotorR);
    }
  }
  ParseIMC(&control);
}
