#include "rf.h"

XBeeWithCallbacks xbee;
FastCRC8 CRC8;

void(* resetFunc) (void) = 0;         //declare reset function at address 0

void InitXBEE(Controller *tm) {
  XReset();                           //Reset Xbee Here
  delay(2000);
  Serial.begin(38400);                //Setup Serial for Xbee 38400
  xbee.setSerial(Serial);
  xbee.onZBRxResponse(Receive , (uintptr_t)tm);
}


void Receive(ZBRxResponse& rx, uintptr_t t) {
  uint8_t Data[150] = {0}, CRC = 0;
  uint8_t DataLength = 0;
  Controller *tm = (Controller*)t;
  DataLength = rx.getDataLength();
  if ( DataLength < 150) {
    for (int i = 0; i < DataLength; i++) {
      Data[i] = rx.getData()[i];
    }
    if (Data[0] == PACKETHEADER) {
      CRC = CRC8.smbus(Data , rx.getDataLength() - 1);    //CRCs
      //Process Data Here
      if (CRC == Data[rx.getDataLength() - 1]) {
        if (Data[1] == MANUALCONTROL) {
          tm->LastUpdate = millis();
          tm->LastHeartBeat = millis();
          tm->MotorL = (word(Data[2], Data[3]));
          tm->MotorR = (word(Data[4], Data[5]));
          tm->EStop = Data[6];
          tm->Mode = MANUAL;
          tm->ResponseID = rx.getRemoteAddress64();
          tm->SendResponse++;
        }
      }
    }  else if (Data[0] == RESETCONTROLLER) {
      resetFunc();
    } else if (Data[0] == 0x11 && Data[1] == 0x22) {
      //HeartBeat
      tm->LastHeartBeat = millis();
    } else if (Data[0] == 0x54 && Data[1] == 0xfe) {
      //IMC Message
      tm->IMCLength = DataLength;
      tm->BaseID = rx.getRemoteAddress64();
      tm->SendIMC = 1;
      for (uint8_t i = 0; i < DataLength ; i++) {
        tm->RFData[i] = Data[i];
      }
    }
  }
}

void Transmit(uint64_t Address , uint8_t *Data , uint8_t Length) {
  ZBTxRequest tx;
  tx.setAddress64(Address);
  tx.setPayload(Data , Length);
  xbee.send(tx);
}

void XLoop() {
  xbee.loop();
}

void XReset() {
  pinMode(XRST , OUTPUT);
  digitalWrite(XRST , LOW);
  delay(1000);
  pinMode(XRST , INPUT);
}
