/*
 * board.h
 *
 *  Created on: Oct 22, 2018
 *      Author: llewellyn
 */

#ifndef SRC_RF_H_
#define SRC_RF_H_

#include <XBee.h>
#include <FastCRC.h>
#include "controller.h"


#define RESETCONTROLLER     0xCC
#define PACKETHEADER        0x3F 
#define MANUALCONTROL       0xAB 

void InitXBEE(Controller *tm);
void Receive(ZBRxResponse& rx, uintptr_t t);
void Transmit(uint64_t Address , uint8_t *Data , uint8_t Length);
void XLoop();
void XReset();

extern NeoSWSerial mySerial;
extern void analogWrite16(uint8_t pin, uint16_t val);

#endif /* SRC_RF_H_ */
