/*
   board.h

    Created on: Oct 22, 2018
        Author: llewellyn
*/

#ifndef SRC_CONTROLLER_H_
#define SRC_CONTROLLER_H_

#include "board.h"
#include <NeoSWSerial.h>
#include <XBee.h>

#define DUNE_IMC_CONST_SYNC               0xFE54
#define DUNE_IMC_CONST_SYNC_REV           0x54FE
#define DUNE_IMC_CONST_HEADER_SIZE        20
#define DUNE_IMC_LENGTH_LOW_POSITION      4
#define DUNE_IMC_LENGTH_HIGH_POSITION     5
#define DUNE_IMC_CONST_FOOTER_SIZE        2

typedef struct {
  uint8_t Mode;                    //Mode of the Controller
  unsigned long LastUpdate;        //Last Manual Control message
  unsigned long LastHeartBeat;     //Last HeartBeat message from HandHeld
  uint8_t EStop;                   //Values ESTOP switch
  uint16_t MotorL , MotorR;        //values for Left and Right motor
  XBeeAddress64 ResponseID;        //Last Handheld controller Address
  uint8_t SendResponse;            //Byte to sennd response
  uint8_t RFData[100];             //Buffer to Send RF Data
  unsigned long LastIMC;           //Time for Last IMC Message
  XBeeAddress64 BaseID;            //Last Base Station Address
  uint8_t SendIMC;                 //Byte to Send IMC Message
  uint8_t IMCLength;               //IMC Message Length
} Controller;

enum {
  c_sync,
  c_header,
  c_payload,
  c_footer
} ParserStage;                      //enum for IMC Parser

static inline uint16_t compute(const uint8_t* buffer, uint16_t len);
void ParseIMC(Controller *tm);
void InitHardware();
void setupPWM16();
void analogWrite16(uint8_t pin, uint16_t val);
extern void Transmit(uint64_t Address , uint8_t *Data , uint8_t Length);

#endif /* SRC_CONTROLLER_H_ */
