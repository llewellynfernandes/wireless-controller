/*
 * board.h
 *
 *  Created on: Oct 22, 2018
 *      Author: llewellyn
 */

#ifndef SRC_BOARD_H_
#define SRC_BOARD_H_

#define VERSION           1.0
#define LEFTMOTOR         9
#define RIGHTMOTOR        10
#define MANUALEN          5
#define CONNECT           6
#define XRST              A5
#define STX               2
#define SRX               3
#define AUTO              10
#define MANUAL            11

#endif /* SRC_BOARD_H_ */
